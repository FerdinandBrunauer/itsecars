using System.Linq;
using System.Threading.Tasks;
using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities.Base;
using ItsECars.Service.Repositories.Base;
using NUnit.Framework;

namespace ItsECars.Service.Test.Controllers
{
	public class PaginationTest
	{
		private readonly TestController testController = new TestController(new TestEntityRepository());

		[Test]
		public void DefaultValueTest()
		{
			Assert.That(
				this.testController.GetAllEntities(new Pagination()),
				Is.EquivalentTo(Enumerable.Range(1, 20).Select(n => new TestEntity(n)))
			);
		}

		[Test]
		public void ChangedPageTest()
		{
			Assert.That(
				this.testController.GetAllEntities(new Pagination { Page = 1 }),
				Is.EquivalentTo(Enumerable.Range(21, 20).Select(n => new TestEntity(n)))
			);
		}

		[Test]
		public void ChangedPerPageTest()
		{
			Assert.That(
				this.testController.GetAllEntities(new Pagination { PerPage = 1 }),
				Has.Exactly(1).Items
			);
		}

		private class TestEntity : IEntity
		{
			public TestEntity(long id)
			{
				this.Id = id;
			}

			/// <inheritdoc />
			public long Id { get; }

			private bool Equals(TestEntity other)
			{
				return this.Id == other.Id;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return this.Equals((TestEntity) obj);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				return this.Id.GetHashCode();
			}
		}

		private class TestEntityRepository : IGenericRepository<TestEntity>
		{
			/// <inheritdoc />
			public IQueryable<TestEntity> GetAll()
			{
				return Enumerable.Range(1, 100).Select(n => new TestEntity(n)).AsQueryable();
			}

			/// <inheritdoc />
			public Task<TestEntity> GetById(long id)
			{
				throw new System.NotImplementedException();
			}

			/// <inheritdoc />
			public Task Create(TestEntity entity)
			{
				throw new System.NotImplementedException();
			}

			/// <inheritdoc />
			public Task Update(long id, TestEntity entity)
			{
				throw new System.NotImplementedException();
			}

			/// <inheritdoc />
			public Task Delete(long id)
			{
				throw new System.NotImplementedException();
			}
		}

		private class TestController : APagedCrudController<TestEntity, TestEntityRepository>
		{
			/// <inheritdoc />
			public TestController(TestEntityRepository entityRepository) : base(entityRepository)
			{
			}
		}
	}
}