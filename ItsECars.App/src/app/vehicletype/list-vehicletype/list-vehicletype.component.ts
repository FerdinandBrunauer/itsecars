import { Component, OnInit } from '@angular/core';
import { Vehicletype } from 'src/app/api/models';
import { VehicletypeService } from 'src/app/api/services';
import { MatDialog } from '@angular/material';
import { EditVehicletypeComponent } from '../edit-vehicletype/edit-vehicletype.component';

@Component({
  selector: 'app-list-vehicletype',
  templateUrl: './list-vehicletype.component.html',
  styleUrls: ['./list-vehicletype.component.css']
})
export class ListVehicletypeComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'seatPlaces', 'actions'];
  items: Vehicletype[];

  constructor(public dialog: MatDialog, public vehicletypeService: VehicletypeService) {
  }

  ngOnInit() {
    this.vehicletypeService
      .GetAllEntities({})
      .subscribe(types => (this.items = types));
  }

  AddItem(): void {
    const vehicletype: Vehicletype = {
      name: '',
      seatPlaces: 0
    };
    const dialogRef = this.dialog.open(EditVehicletypeComponent, { data: vehicletype });
    dialogRef.afterClosed().subscribe(result => {
      this.vehicletypeService.CreateEntity(vehicletype).subscribe(_ => this.items = this.items.concat(vehicletype));
    });
  }

  EditItem(vehicletype: Vehicletype): void {
    const vehicletypeEdit = { ...vehicletype };
    const dialogRef = this.dialog.open(EditVehicletypeComponent, { data: vehicletypeEdit });
    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }

      this.vehicletypeService.UpdateEntity({ id: vehicletype.id, entity: vehicletypeEdit }).subscribe(
        _ =>
          this.items = this.items
            .filter(i => i.id !== vehicletype.id)
            .concat(vehicletypeEdit)
            .sort((a, b) => a.id > b.id ? 1 : -1)
      );
    });
  }

  DeleteItem(vehicletype: Vehicletype): void {
    this.vehicletypeService
      .DeleteEntity(vehicletype.id)
      .subscribe(
        _ => (this.items = this.items.filter(i => i.id !== vehicletype.id))
      );
  }
}
