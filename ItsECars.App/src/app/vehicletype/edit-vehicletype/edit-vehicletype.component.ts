import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Vehicletype } from 'src/app/api/models';

@Component({
  selector: 'app-edit-vehicletype',
  templateUrl: './edit-vehicletype.component.html',
  styleUrls: ['./edit-vehicletype.component.css']
})
export class EditVehicletypeComponent {
  constructor(
    public dialogRef: MatDialogRef<EditVehicletypeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Vehicletype) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
