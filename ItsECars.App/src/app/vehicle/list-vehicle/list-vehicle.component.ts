import { Component } from '@angular/core';
import { Vehicle } from 'src/app/api/models';
import { VehicleService } from 'src/app/api/services';

@Component({
  selector: 'app-list-vehicle',
  templateUrl: './list-vehicle.component.html',
  styleUrls: ['./list-vehicle.component.css']
})
export class ListVehicleComponent {
  displayedColumns: string[] = [
    'id',
    'currentMileage',
    'fixCostPerRental',
    'costPerMileage',
    'costPerDay',
    'numberPlate',
    'currentRentalLocation',
    'ownerRentalLocation',
    'vehicletype'
  ];
  items: Vehicle[];

  constructor(vehicleService: VehicleService) {
    vehicleService
      .GetAllEntities({})
      .subscribe(vehicles => (this.items = vehicles));
  }
}
