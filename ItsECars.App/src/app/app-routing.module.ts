import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListRentallocationComponent } from './rentallocation/list-rentallocation/list-rentallocation.component';
import { ListVehicleComponent } from './vehicle/list-vehicle/list-vehicle.component';
import { ListVehicletypeComponent } from './vehicletype/list-vehicletype/list-vehicletype.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'rentallocation', component: ListRentallocationComponent },
  { path: 'vehicle', component: ListVehicleComponent },
  { path: 'vehicletype', component: ListVehicletypeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
