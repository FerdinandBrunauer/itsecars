import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatButtonModule, MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { ListVehicletypeComponent } from './vehicletype/list-vehicletype/list-vehicletype.component';
import { ListVehicleComponent } from './vehicle/list-vehicle/list-vehicle.component';
import { ListRentallocationComponent } from './rentallocation/list-rentallocation/list-rentallocation.component';
import { ApiModule } from './api/api.module';
import { EditVehicletypeComponent } from './vehicletype/edit-vehicletype/edit-vehicletype.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  entryComponents: [EditVehicletypeComponent],
  declarations: [
    AppComponent,
    ListVehicletypeComponent,
    ListVehicleComponent,
    ListRentallocationComponent,
    EditVehicletypeComponent
  ],
  imports: [
    ApiModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
