/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { FlatRate } from '../models/flat-rate';
@Injectable({
  providedIn: 'root',
})
class FlatRateService extends __BaseService {
  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `FlatRateService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntitiesResponse(params: FlatRateService.GetAllEntitiesParams): __Observable<__StrictHttpResponse<Array<FlatRate>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.perPage != null) __params = __params.set('per_page', params.perPage.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/FlatRate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<FlatRate>>;
      })
    );
  }
  /**
   * @param params The `FlatRateService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntities(params: FlatRateService.GetAllEntitiesParams): __Observable<Array<FlatRate>> {
    return this.GetAllEntitiesResponse(params).pipe(
      __map(_r => _r.body as Array<FlatRate>)
    );
  }

  /**
   * @param entity undefined
   */
  CreateEntityResponse(entity?: FlatRate): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/FlatRate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param entity undefined
   */
  CreateEntity(entity?: FlatRate): __Observable<null> {
    return this.CreateEntityResponse(entity).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   * @return Success
   */
  GetEntityByIdResponse(id: number): __Observable<__StrictHttpResponse<FlatRate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/FlatRate/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FlatRate>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  GetEntityById(id: number): __Observable<FlatRate> {
    return this.GetEntityByIdResponse(id).pipe(
      __map(_r => _r.body as FlatRate)
    );
  }

  /**
   * @param params The `FlatRateService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntityResponse(params: FlatRateService.UpdateEntityParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/FlatRate/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `FlatRateService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity(params: FlatRateService.UpdateEntityParams): __Observable<null> {
    return this.UpdateEntityResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `FlatRateService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1Response(params: FlatRateService.UpdateEntity_1Params): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/FlatRate/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `FlatRateService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1(params: FlatRateService.UpdateEntity_1Params): __Observable<null> {
    return this.UpdateEntity_1Response(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  DeleteEntityResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/FlatRate/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  DeleteEntity(id: number): __Observable<null> {
    return this.DeleteEntityResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module FlatRateService {

  /**
   * Parameters for GetAllEntities
   */
  export interface GetAllEntitiesParams {

    /**
     * Items per page
     */
    perPage?: number;

    /**
     * Current page number
     */
    page?: number;
  }

  /**
   * Parameters for UpdateEntity
   */
  export interface UpdateEntityParams {
    id: number;
    entity?: FlatRate;
  }

  /**
   * Parameters for UpdateEntity_1
   */
  export interface UpdateEntity_1Params {
    id: number;
    entity?: FlatRate;
  }
}

export { FlatRateService }
