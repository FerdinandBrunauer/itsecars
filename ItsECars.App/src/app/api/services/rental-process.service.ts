/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RentalProcess } from '../models/rental-process';
@Injectable({
  providedIn: 'root',
})
class RentalProcessService extends __BaseService {
  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `RentalProcessService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntitiesResponse(params: RentalProcessService.GetAllEntitiesParams): __Observable<__StrictHttpResponse<Array<RentalProcess>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.perPage != null) __params = __params.set('per_page', params.perPage.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/RentalProcess`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RentalProcess>>;
      })
    );
  }
  /**
   * @param params The `RentalProcessService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntities(params: RentalProcessService.GetAllEntitiesParams): __Observable<Array<RentalProcess>> {
    return this.GetAllEntitiesResponse(params).pipe(
      __map(_r => _r.body as Array<RentalProcess>)
    );
  }

  /**
   * @param entity undefined
   */
  CreateEntityResponse(entity?: RentalProcess): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/RentalProcess`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param entity undefined
   */
  CreateEntity(entity?: RentalProcess): __Observable<null> {
    return this.CreateEntityResponse(entity).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   * @return Success
   */
  GetEntityByIdResponse(id: number): __Observable<__StrictHttpResponse<RentalProcess>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/RentalProcess/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RentalProcess>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  GetEntityById(id: number): __Observable<RentalProcess> {
    return this.GetEntityByIdResponse(id).pipe(
      __map(_r => _r.body as RentalProcess)
    );
  }

  /**
   * @param params The `RentalProcessService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntityResponse(params: RentalProcessService.UpdateEntityParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/RentalProcess/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `RentalProcessService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity(params: RentalProcessService.UpdateEntityParams): __Observable<null> {
    return this.UpdateEntityResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `RentalProcessService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1Response(params: RentalProcessService.UpdateEntity_1Params): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/RentalProcess/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `RentalProcessService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1(params: RentalProcessService.UpdateEntity_1Params): __Observable<null> {
    return this.UpdateEntity_1Response(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  DeleteEntityResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/RentalProcess/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  DeleteEntity(id: number): __Observable<null> {
    return this.DeleteEntityResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module RentalProcessService {

  /**
   * Parameters for GetAllEntities
   */
  export interface GetAllEntitiesParams {

    /**
     * Items per page
     */
    perPage?: number;

    /**
     * Current page number
     */
    page?: number;
  }

  /**
   * Parameters for UpdateEntity
   */
  export interface UpdateEntityParams {
    id: number;
    entity?: RentalProcess;
  }

  /**
   * Parameters for UpdateEntity_1
   */
  export interface UpdateEntity_1Params {
    id: number;
    entity?: RentalProcess;
  }
}

export { RentalProcessService }
