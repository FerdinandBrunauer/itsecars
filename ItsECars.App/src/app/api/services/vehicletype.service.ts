/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Vehicletype } from '../models/vehicletype';
@Injectable({
  providedIn: 'root',
})
class VehicletypeService extends __BaseService {
  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `VehicletypeService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntitiesResponse(params: VehicletypeService.GetAllEntitiesParams): __Observable<__StrictHttpResponse<Array<Vehicletype>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.perPage != null) __params = __params.set('per_page', params.perPage.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicletype`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Vehicletype>>;
      })
    );
  }
  /**
   * @param params The `VehicletypeService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntities(params: VehicletypeService.GetAllEntitiesParams): __Observable<Array<Vehicletype>> {
    return this.GetAllEntitiesResponse(params).pipe(
      __map(_r => _r.body as Array<Vehicletype>)
    );
  }

  /**
   * @param entity undefined
   */
  CreateEntityResponse(entity?: Vehicletype): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/Vehicletype`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param entity undefined
   */
  CreateEntity(entity?: Vehicletype): __Observable<null> {
    return this.CreateEntityResponse(entity).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   * @return Success
   */
  GetEntityByIdResponse(id: number): __Observable<__StrictHttpResponse<Vehicletype>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicletype/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Vehicletype>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  GetEntityById(id: number): __Observable<Vehicletype> {
    return this.GetEntityByIdResponse(id).pipe(
      __map(_r => _r.body as Vehicletype)
    );
  }

  /**
   * @param params The `VehicletypeService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntityResponse(params: VehicletypeService.UpdateEntityParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/Vehicletype/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `VehicletypeService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity(params: VehicletypeService.UpdateEntityParams): __Observable<null> {
    return this.UpdateEntityResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `VehicletypeService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1Response(params: VehicletypeService.UpdateEntity_1Params): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Vehicletype/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `VehicletypeService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1(params: VehicletypeService.UpdateEntity_1Params): __Observable<null> {
    return this.UpdateEntity_1Response(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  DeleteEntityResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicletype/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  DeleteEntity(id: number): __Observable<null> {
    return this.DeleteEntityResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module VehicletypeService {

  /**
   * Parameters for GetAllEntities
   */
  export interface GetAllEntitiesParams {

    /**
     * Items per page
     */
    perPage?: number;

    /**
     * Current page number
     */
    page?: number;
  }

  /**
   * Parameters for UpdateEntity
   */
  export interface UpdateEntityParams {
    id: number;
    entity?: Vehicletype;
  }

  /**
   * Parameters for UpdateEntity_1
   */
  export interface UpdateEntity_1Params {
    id: number;
    entity?: Vehicletype;
  }
}

export { VehicletypeService }
