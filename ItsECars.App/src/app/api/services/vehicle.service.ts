/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Vehicle } from '../models/vehicle';
@Injectable({
  providedIn: 'root',
})
class VehicleService extends __BaseService {
  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `VehicleService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntitiesResponse(params: VehicleService.GetAllEntitiesParams): __Observable<__StrictHttpResponse<Array<Vehicle>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.perPage != null) __params = __params.set('per_page', params.perPage.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicle`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<Vehicle>>;
      })
    );
  }
  /**
   * @param params The `VehicleService.GetAllEntitiesParams` containing the following parameters:
   *
   * - `per_page`: Items per page
   *
   * - `page`: Current page number
   *
   * @return Success
   */
  GetAllEntities(params: VehicleService.GetAllEntitiesParams): __Observable<Array<Vehicle>> {
    return this.GetAllEntitiesResponse(params).pipe(
      __map(_r => _r.body as Array<Vehicle>)
    );
  }

  /**
   * @param entity undefined
   */
  CreateEntityResponse(entity?: Vehicle): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/Vehicle`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param entity undefined
   */
  CreateEntity(entity?: Vehicle): __Observable<null> {
    return this.CreateEntityResponse(entity).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   * @return Success
   */
  GetEntityByIdResponse(id: number): __Observable<__StrictHttpResponse<Vehicle>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicle/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Vehicle>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  GetEntityById(id: number): __Observable<Vehicle> {
    return this.GetEntityByIdResponse(id).pipe(
      __map(_r => _r.body as Vehicle)
    );
  }

  /**
   * @param params The `VehicleService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntityResponse(params: VehicleService.UpdateEntityParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/Vehicle/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `VehicleService.UpdateEntityParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity(params: VehicleService.UpdateEntityParams): __Observable<null> {
    return this.UpdateEntityResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `VehicleService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1Response(params: VehicleService.UpdateEntity_1Params): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.entity;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Vehicle/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `VehicleService.UpdateEntity_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `entity`:
   */
  UpdateEntity_1(params: VehicleService.UpdateEntity_1Params): __Observable<null> {
    return this.UpdateEntity_1Response(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  DeleteEntityResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Vehicle/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  DeleteEntity(id: number): __Observable<null> {
    return this.DeleteEntityResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module VehicleService {

  /**
   * Parameters for GetAllEntities
   */
  export interface GetAllEntitiesParams {

    /**
     * Items per page
     */
    perPage?: number;

    /**
     * Current page number
     */
    page?: number;
  }

  /**
   * Parameters for UpdateEntity
   */
  export interface UpdateEntityParams {
    id: number;
    entity?: Vehicle;
  }

  /**
   * Parameters for UpdateEntity_1
   */
  export interface UpdateEntity_1Params {
    id: number;
    entity?: Vehicle;
  }
}

export { VehicleService }
