/* tslint:disable */
import { Injectable } from "@angular/core";

/**
 * Global configuration for Api services
 */
@Injectable({
  providedIn: "root"
})
export class ApiConfiguration {
  rootUrl: string = "http://35.228.91.96:8080";
}
