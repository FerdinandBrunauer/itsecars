/* tslint:disable */
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration } from './api-configuration';

import { CustomerService } from './services/customer.service';
import { DriverLicenseService } from './services/driver-license.service';
import { FlatRateService } from './services/flat-rate.service';
import { RentalLocationService } from './services/rental-location.service';
import { RentalProcessService } from './services/rental-process.service';
import { ReservationService } from './services/reservation.service';
import { VehicleService } from './services/vehicle.service';
import { VehicletypeService } from './services/vehicletype.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    CustomerService,
    DriverLicenseService,
    FlatRateService,
    RentalLocationService,
    RentalProcessService,
    ReservationService,
    VehicleService,
    VehicletypeService
  ],
})
export class ApiModule { }
