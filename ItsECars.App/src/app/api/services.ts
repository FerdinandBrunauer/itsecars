export { CustomerService } from './services/customer.service';
export { DriverLicenseService } from './services/driver-license.service';
export { FlatRateService } from './services/flat-rate.service';
export { RentalLocationService } from './services/rental-location.service';
export { RentalProcessService } from './services/rental-process.service';
export { ReservationService } from './services/reservation.service';
export { VehicleService } from './services/vehicle.service';
export { VehicletypeService } from './services/vehicletype.service';
