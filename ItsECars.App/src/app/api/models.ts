export { Customer } from './models/customer';
export { DriverLicense } from './models/driver-license';
export { Vehicletype } from './models/vehicletype';
export { FlatRate } from './models/flat-rate';
export { RentalLocation } from './models/rental-location';
export { RentalProcess } from './models/rental-process';
export { Vehicle } from './models/vehicle';
export { Reservation } from './models/reservation';
