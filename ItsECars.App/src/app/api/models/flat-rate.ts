/* tslint:disable */

/**
 * Booked and payed flat rate of a ItsECars.Service.Entities.Reservation
 */
export interface FlatRate {
  id?: number;

  /**
   * Name of the ItsECars.Service.Entities.FlatRate
   */
  name?: string;

  /**
   * Included mileage in the ItsECars.Service.Entities.FlatRate
   */
  includedMileage?: number;

  /**
   * Included days in the ItsECars.Service.Entities.FlatRate
   */
  includedDays?: number;
}
