/* tslint:disable */
import { RentalLocation } from './rental-location';
import { Vehicletype } from './vehicletype';

/**
 * Represents a bookable ItsECars.Service.Entities.Vehicle
 */
export interface Vehicle {
  id?: number;

  /**
   * Current mileage of the ItsECars.Service.Entities.Vehicle
   */
  currentMileage?: number;

  /**
   * Fix costs per rent
   */
  fixCostPerRental?: number;

  /**
   * Cost per kilometer during a rent
   */
  costPerMileage?: number;

  /**
   * Cost per day during a rent
   */
  costPerDay?: number;

  /**
   * Number plate of the ItsECars.Service.Entities.Vehicle
   */
  numberPlate?: string;

  /**
   * Current location of the ItsECars.Service.Entities.Vehicle
   * May be null if it is currently not available
   */
  currentRentalLocation?: RentalLocation;

  /**
   * Origin location of the ItsECars.Service.Entities.VehicleItsECars.Service.Entities.Vehicle should always be there if not booked
   */
  ownerRentalLocation?: RentalLocation;

  /**
   * Type of the ItsECars.Service.Entities.Vehicle
   */
  vehicletype?: Vehicletype;
}
