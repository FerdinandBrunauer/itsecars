/* tslint:disable */
import { Vehicletype } from './vehicletype';
import { Customer } from './customer';

/**
 * Represents a ItsECars.Service.Entities.DriverLicense of a ItsECars.Service.Entities.DriverLicense.Customer
 */
export interface DriverLicense {
  id?: number;

  /**
   * Driver license checked by
   */
  checkedBy?: string;

  /**
   * Driver license checked at
   */
  checkedOn?: string;

  /**
   * Allowed ItsECars.Service.Entities.DriverLicense.Vehicletype with this ItsECars.Service.Entities.DriverLicense
   */
  vehicletype?: Vehicletype;

  /**
   * ItsECars.Service.Entities.DriverLicense.Customer of this ItsECars.Service.Entities.DriverLicense
   */
  customer?: Customer;
}
