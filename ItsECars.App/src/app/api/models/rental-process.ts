/* tslint:disable */
import { RentalLocation } from './rental-location';
import { Vehicle } from './vehicle';
import { Customer } from './customer';
import { Reservation } from './reservation';

/**
 * Represents the rental of a ItsECars.Service.Entities.RentalProcess.Vehicle
 * May be the result of a previous ItsECars.Service.Entities.Reservation
 */
export interface RentalProcess {
  id?: number;

  /**
   * Start time of the ItsECars.Service.Entities.RentalProcess
   */
  startTime?: string;

  /**
   * End time of the ItsECars.Service.Entities.RentalProcess
   * May be null if the ItsECars.Service.Entities.RentalProcess is not finished yet
   */
  endTime?: string;

  /**
   * Start mileage of the ItsECars.Service.Entities.RentalProcess.Vehicle
   */
  startMileage?: number;

  /**
   * End mileage of the ItsECars.Service.Entities.RentalProcess.Vehicle
   */
  endMileage?: number;

  /**
   * Start location
   */
  startLocation?: RentalLocation;

  /**
   * End location
   */
  endLocation?: RentalLocation;

  /**
   * Rented ItsECars.Service.Entities.RentalProcess.Vehicle
   */
  vehicle?: Vehicle;

  /**
   * ItsECars.Service.Entities.RentalProcess.Customer who initiated this ItsECars.Service.Entities.RentalProcess
   */
  customer?: Customer;

  /**
   * Optional previous ItsECars.Service.Entities.ReservationItsECars.Service.Entities.Reservation may already include payed ItsECars.Service.Entities.FlatRate
   */
  reservation?: Reservation;
}
