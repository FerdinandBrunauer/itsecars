/* tslint:disable */
import { RentalLocation } from './rental-location';
import { Vehicletype } from './vehicletype';
import { FlatRate } from './flat-rate';
import { Customer } from './customer';
import { RentalProcess } from './rental-process';

/**
 * Represents the interest of a ItsECars.Service.Entities.Reservation.Customer in a ItsECars.Service.Entities.Vehicletype
 * May result in a ItsECars.Service.Entities.Reservation.RentalProcess
 */
export interface Reservation {
  id?: number;

  /**
   * Requested start time
   */
  startTime?: string;

  /**
   * Requested end time
   */
  endTime?: string;

  /**
   * Start location of the ItsECars.Service.Entities.Reservation
   */
  startLocation?: RentalLocation;

  /**
   * End location of the ItsECars.Service.Entities.Reservation
   */
  endLocation?: RentalLocation;

  /**
   * Requested ItsECars.Service.Entities.Vehicletype
   */
  requestedVehicletype?: Vehicletype;

  /**
   * Already payed ItsECars.Service.Entities.FlatRate
   */
  payedFlatRate?: FlatRate;

  /**
   * Reservation done by ItsECars.Service.Entities.Reservation.Customer
   */
  customer?: Customer;

  /**
   * Resulting ItsECars.Service.Entities.Reservation.RentalProcess
   */
  rentalProcess?: RentalProcess;
}
