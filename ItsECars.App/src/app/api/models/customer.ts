/* tslint:disable */

/**
 * Represents a ItsECars.Service.Entities.Customer which can book ItsECars.Service.Entities.Vehicle
 */
export interface Customer {
  id?: number;

  /**
   * Firstname of the ItsECars.Service.Entities.Customer
   */
  firstname?: string;

  /**
   * Lastname of the ItsECars.Service.Entities.Customer
   */
  lastname?: string;

  /**
   * Birthdate of the ItsECars.Service.Entities.Customer
   */
  birthdate?: string;

  /**
   * Living country of the ItsECars.Service.Entities.Customer
   */
  country?: string;

  /**
   * Living street name of the ItsECars.Service.Entities.Customer
   */
  streetName?: string;

  /**
   * Living street number of the ItsECars.Service.Entities.Customer
   */
  streetNumber?: string;

  /**
   * Living postal code of the ItsECars.Service.Entities.Customer
   */
  postalCode?: string;

  /**
   * Living vilage name of the ItsECars.Service.Entities.Customer
   */
  city?: string;
}
