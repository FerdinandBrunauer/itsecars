/* tslint:disable */

/**
 * Represents a ItsECars.Service.Entities.RentalLocation where ItsECars.Service.Entities.Vehicle can be picked up or brought back
 */
export interface RentalLocation {
  id?: number;

  /**
   * Textual representation of the ItsECars.Service.Entities.RentalLocation
   */
  name?: string;

  /**
   * Living country of the ItsECars.Service.Entities.Customer
   */
  country?: string;

  /**
   * Living street name of the ItsECars.Service.Entities.Customer
   */
  streetName?: string;

  /**
   * Living street number of the ItsECars.Service.Entities.Customer
   */
  streetNumber?: string;

  /**
   * Living postal code of the ItsECars.Service.Entities.Customer
   */
  postalCode?: string;

  /**
   * Living vilage name of the ItsECars.Service.Entities.Customer
   */
  city?: string;
}
