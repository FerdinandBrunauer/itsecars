/* tslint:disable */

/**
 * Represents a type of a ItsECars.Service.Entities.Vehicle
 */
export interface Vehicletype {
  id?: number;

  /**
   * Name of the type
   */
  name?: string;

  /**
   * Minimum number of seat places in this type
   */
  seatPlaces?: number;
}
