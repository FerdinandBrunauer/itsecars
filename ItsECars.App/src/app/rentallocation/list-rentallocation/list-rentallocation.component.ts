import { Component } from '@angular/core';
import { RentalLocation } from 'src/app/api/models';
import { RentalLocationService } from 'src/app/api/services';

@Component({
  selector: 'app-list-rentallocation',
  templateUrl: './list-rentallocation.component.html',
  styleUrls: ['./list-rentallocation.component.css']
})
export class ListRentallocationComponent {
  displayedColumns: string[] = [
    'id',
    'name',
    'country',
    'streetName',
    'streetNumber',
    'postalCode',
    'city'
  ];
  items: RentalLocation[];

  constructor(rentalLocationService: RentalLocationService) {
    rentalLocationService
      .GetAllEntities({})
      .subscribe(locations => (this.items = locations));
  }
}
