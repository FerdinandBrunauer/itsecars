﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ItsECars.Service
{
	/// <summary>
	/// Main class of the application. Used to initialize
	/// </summary>
	public static class Program
	{
		/// <summary>
		/// Main entry point
		/// </summary>
		public static void Main(string[] args)
		{
			Program.CreateWebHostBuilder(args)
				.Build()
				.Run();
		}

		private static IWebHostBuilder CreateWebHostBuilder(string[] args)
		{
			var configuration = new ConfigurationBuilder()
				.AddCommandLine(args)
				.Build();

			return WebHost.CreateDefaultBuilder(args)
				.UseConfiguration(configuration)
				.UseStartup<Startup>();
		}
	}
}
