﻿using System;
using ItsECars.Service.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

#pragma warning disable CS1591 // Fehledes XML-Kommentar für öffentlich sichtbaren Typ oder Element

namespace ItsECars.Service.Migrations
{
	[DbContext(typeof(ItsECarsDatabaseContext))]
	[Migration("20181215202357_InitialCreate")]
	partial class InitialCreate
	{
		protected override void BuildTargetModel(ModelBuilder modelBuilder)
		{
			modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");
			modelBuilder.Entity("ItsECars.Service.Entities.Customer", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<DateTime>("Birthdate");
				b.Property<string>("City");
				b.Property<string>("Country");
				b.Property<string>("Firstname");
				b.Property<string>("Lastname");
				b.Property<string>("PostalCode");
				b.Property<string>("StreetName");
				b.Property<string>("StreetNumber");
				b.HasKey("Id");
				b.HasIndex("Id");
				b.ToTable("Customers");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.DriverLicense", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<string>("CheckedBy");
				b.Property<DateTime?>("CheckedOn");
				b.Property<long?>("CustomerId");
				b.Property<long?>("VehicletypeId");
				b.HasKey("Id");
				b.HasIndex("CustomerId");
				b.HasIndex("Id");
				b.HasIndex("VehicletypeId");
				b.ToTable("DriverLicenses");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.FlatRate", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<int>("IncludedDays");
				b.Property<int>("IncludedMileage");
				b.Property<string>("Name");
				b.HasKey("Id");
				b.HasIndex("Id");
				b.ToTable("FlatRates");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.RentalLocation", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<string>("City");
				b.Property<string>("Country");
				b.Property<string>("Name");
				b.Property<string>("PostalCode");
				b.Property<string>("StreetName");
				b.Property<string>("StreetNumber");
				b.HasKey("Id");
				b.HasIndex("Id");
				b.ToTable("RentalLocations");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.RentalProcess", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<long?>("CustomerId");
				b.Property<long?>("EndLocationId");
				b.Property<int?>("EndMileage");
				b.Property<DateTime?>("EndTime");
				b.Property<long?>("StartLocationId");
				b.Property<int>("StartMileage");
				b.Property<DateTime>("StartTime");
				b.Property<long?>("VehicleId");
				b.HasKey("Id");
				b.HasIndex("CustomerId");
				b.HasIndex("EndLocationId");
				b.HasIndex("Id");
				b.HasIndex("StartLocationId");
				b.HasIndex("VehicleId");
				b.ToTable("RentalProcesses");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.Reservation", b =>
			{
				b.Property<long>("Id");
				b.Property<long?>("CustomerId");
				b.Property<long?>("EndLocationId");
				b.Property<DateTime>("EndTime");
				b.Property<long?>("PayedFlatRateId");
				b.Property<long?>("RequestedVehicletypeId");
				b.Property<long?>("StartLocationId");
				b.Property<DateTime>("StartTime");
				b.HasKey("Id");
				b.HasIndex("CustomerId");
				b.HasIndex("EndLocationId");
				b.HasIndex("Id");
				b.HasIndex("PayedFlatRateId");
				b.HasIndex("RequestedVehicletypeId");
				b.HasIndex("StartLocationId");
				b.ToTable("Reservations");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.Vehicle", b =>
			{
				b.Property<long>("Id").ValueGeneratedOnAdd();
				b.Property<float>("CostPerDay");
				b.Property<float>("CostPerMileage");
				b.Property<int>("CurrentMileage");
				b.Property<long?>("CurrentRentalLocationId");
				b.Property<float>("FixCostPerRental");
				b.Property<string>("NumberPlate");
				b.Property<long?>("OwnerRentalLocationId");
				b.Property<long?>("VehicletypeId");
				b.HasKey("Id");
				b.HasIndex("CurrentRentalLocationId");
				b.HasIndex("Id");
				b.HasIndex("OwnerRentalLocationId");
				b.HasIndex("VehicletypeId");
				b.ToTable("Vehicles");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.Vehicletype", b =>
			{
				b.Property<long>("Id")
					.ValueGeneratedOnAdd();

				b.Property<string>("Name");
				b.Property<int>("SeatPlaces");
				b.HasKey("Id");
				b.HasIndex("Id");
				b.ToTable("Vehicletypes");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.DriverLicense", b =>
			{
				b.HasOne("ItsECars.Service.Entities.Customer", "Customer")
					.WithMany("DriverLicenses")
					.HasForeignKey("CustomerId");

				b.HasOne("ItsECars.Service.Entities.Vehicletype", "Vehicletype")
					.WithMany("DriverLicenses")
					.HasForeignKey("VehicletypeId");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.RentalProcess", b =>
			{
				b.HasOne("ItsECars.Service.Entities.Customer", "Customer")
					.WithMany("RentalProcesses")
					.HasForeignKey("CustomerId");

				b.HasOne("ItsECars.Service.Entities.RentalLocation", "EndLocation")
					.WithMany("EndedRentalProcesses")
					.HasForeignKey("EndLocationId");

				b.HasOne("ItsECars.Service.Entities.RentalLocation", "StartLocation")
					.WithMany("StartedRentalProcesses")
					.HasForeignKey("StartLocationId");

				b.HasOne("ItsECars.Service.Entities.Vehicle", "Vehicle")
					.WithMany("RentalProcesses")
					.HasForeignKey("VehicleId");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.Reservation", b =>
			{
				b.HasOne("ItsECars.Service.Entities.Customer", "Customer")
					.WithMany("Reservations")
					.HasForeignKey("CustomerId");

				b.HasOne("ItsECars.Service.Entities.RentalLocation", "EndLocation")
					.WithMany("EndedReservations")
					.HasForeignKey("EndLocationId");

				b.HasOne("ItsECars.Service.Entities.RentalProcess", "RentalProcess")
					.WithOne("Reservation")
					.HasForeignKey("ItsECars.Service.Entities.Reservation", "Id")
					.OnDelete(DeleteBehavior.Cascade);

				b.HasOne("ItsECars.Service.Entities.FlatRate", "PayedFlatRate")
					.WithMany("Reservations")
					.HasForeignKey("PayedFlatRateId");

				b.HasOne("ItsECars.Service.Entities.Vehicletype", "RequestedVehicletype")
					.WithMany("Reservations")
					.HasForeignKey("RequestedVehicletypeId");

				b.HasOne("ItsECars.Service.Entities.RentalLocation", "StartLocation")
					.WithMany("StartedReservations")
					.HasForeignKey("StartLocationId");
			});

			modelBuilder.Entity("ItsECars.Service.Entities.Vehicle", b =>
			{
				b.HasOne("ItsECars.Service.Entities.RentalLocation", "CurrentRentalLocation")
					.WithMany("CurrentlyAvailableVehicles")
					.HasForeignKey("CurrentRentalLocationId");

				b.HasOne("ItsECars.Service.Entities.RentalLocation", "OwnerRentalLocation")
					.WithMany("OwnedVehicles")
					.HasForeignKey("OwnerRentalLocationId");

				b.HasOne("ItsECars.Service.Entities.Vehicletype", "Vehicletype")
					.WithMany("Vehicles")
					.HasForeignKey("VehicletypeId");
			});
		}
	}
}

#pragma warning restore CS1591 // Fehledes XML-Kommentar für öffentlich sichtbaren Typ oder Element