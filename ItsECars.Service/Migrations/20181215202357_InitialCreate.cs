﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#pragma warning disable CS1591 // Fehledes XML-Kommentar für öffentlich sichtbaren Typ oder Element

namespace ItsECars.Service.Migrations
{
	public partial class InitialCreate : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				"Customers",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					Firstname = table.Column<string>(nullable: true),
					Lastname = table.Column<string>(nullable: true),
					Birthdate = table.Column<DateTime>(nullable: false),
					Country = table.Column<string>(nullable: true),
					StreetName = table.Column<string>(nullable: true),
					StreetNumber = table.Column<string>(nullable: true),
					PostalCode = table.Column<string>(nullable: true),
					City = table.Column<string>(nullable: true)
				},
				constraints: table => { table.PrimaryKey("PK_Customers", x => x.Id); });

			migrationBuilder.CreateTable(
				"FlatRates",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					Name = table.Column<string>(nullable: true),
					IncludedMileage = table.Column<int>(nullable: false),
					IncludedDays = table.Column<int>(nullable: false)
				},
				constraints: table => { table.PrimaryKey("PK_FlatRates", x => x.Id); });

			migrationBuilder.CreateTable(
				"RentalLocations",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					Name = table.Column<string>(nullable: true),
					Country = table.Column<string>(nullable: true),
					StreetName = table.Column<string>(nullable: true),
					StreetNumber = table.Column<string>(nullable: true),
					PostalCode = table.Column<string>(nullable: true),
					City = table.Column<string>(nullable: true)
				},
				constraints: table => { table.PrimaryKey("PK_RentalLocations", x => x.Id); });

			migrationBuilder.CreateTable(
				"Vehicletypes",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					Name = table.Column<string>(nullable: true),
					SeatPlaces = table.Column<int>(nullable: false)
				},
				constraints: table => { table.PrimaryKey("PK_Vehicletypes", x => x.Id); });

			migrationBuilder.CreateTable(
				"DriverLicenses",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					CheckedBy = table.Column<string>(nullable: true),
					CheckedOn = table.Column<DateTime>(nullable: true),
					VehicletypeId = table.Column<long>(nullable: true),
					CustomerId = table.Column<long>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_DriverLicenses", x => x.Id);
					table.ForeignKey(
						"FK_DriverLicenses_Customers_CustomerId",
						x => x.CustomerId,
						"Customers",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_DriverLicenses_Vehicletypes_VehicletypeId",
						x => x.VehicletypeId,
						"Vehicletypes",
						"Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateTable(
				"Vehicles",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					CurrentMileage = table.Column<int>(nullable: false),
					FixCostPerRental = table.Column<float>(nullable: false),
					CostPerMileage = table.Column<float>(nullable: false),
					CostPerDay = table.Column<float>(nullable: false),
					NumberPlate = table.Column<string>(nullable: true),
					CurrentRentalLocationId = table.Column<long>(nullable: true),
					OwnerRentalLocationId = table.Column<long>(nullable: true),
					VehicletypeId = table.Column<long>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Vehicles", x => x.Id);
					table.ForeignKey(
						"FK_Vehicles_RentalLocations_CurrentRentalLocationId",
						x => x.CurrentRentalLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Vehicles_RentalLocations_OwnerRentalLocationId",
						x => x.OwnerRentalLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Vehicles_Vehicletypes_VehicletypeId",
						x => x.VehicletypeId,
						"Vehicletypes",
						"Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateTable(
				"RentalProcesses",
				table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					StartTime = table.Column<DateTime>(nullable: false),
					EndTime = table.Column<DateTime>(nullable: true),
					StartMileage = table.Column<int>(nullable: false),
					EndMileage = table.Column<int>(nullable: true),
					StartLocationId = table.Column<long>(nullable: true),
					EndLocationId = table.Column<long>(nullable: true),
					VehicleId = table.Column<long>(nullable: true),
					CustomerId = table.Column<long>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_RentalProcesses", x => x.Id);
					table.ForeignKey(
						"FK_RentalProcesses_Customers_CustomerId",
						x => x.CustomerId,
						"Customers",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_RentalProcesses_RentalLocations_EndLocationId",
						x => x.EndLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_RentalProcesses_RentalLocations_StartLocationId",
						x => x.StartLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_RentalProcesses_Vehicles_VehicleId",
						x => x.VehicleId,
						"Vehicles",
						"Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateTable(
				"Reservations",
				table => new
				{
					Id = table.Column<long>(nullable: false),
					StartTime = table.Column<DateTime>(nullable: false),
					EndTime = table.Column<DateTime>(nullable: false),
					StartLocationId = table.Column<long>(nullable: true),
					EndLocationId = table.Column<long>(nullable: true),
					RequestedVehicletypeId = table.Column<long>(nullable: true),
					PayedFlatRateId = table.Column<long>(nullable: true),
					CustomerId = table.Column<long>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Reservations", x => x.Id);
					table.ForeignKey(
						"FK_Reservations_Customers_CustomerId",
						x => x.CustomerId,
						"Customers",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Reservations_RentalLocations_EndLocationId",
						x => x.EndLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Reservations_RentalProcesses_Id",
						x => x.Id,
						"RentalProcesses",
						"Id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						"FK_Reservations_FlatRates_PayedFlatRateId",
						x => x.PayedFlatRateId,
						"FlatRates",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Reservations_Vehicletypes_RequestedVehicletypeId",
						x => x.RequestedVehicletypeId,
						"Vehicletypes",
						"Id",
						onDelete: ReferentialAction.Restrict);
					table.ForeignKey(
						"FK_Reservations_RentalLocations_StartLocationId",
						x => x.StartLocationId,
						"RentalLocations",
						"Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateIndex(
				"IX_Customers_Id",
				"Customers",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_DriverLicenses_CustomerId",
				"DriverLicenses",
				"CustomerId");

			migrationBuilder.CreateIndex(
				"IX_DriverLicenses_Id",
				"DriverLicenses",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_DriverLicenses_VehicletypeId",
				"DriverLicenses",
				"VehicletypeId");

			migrationBuilder.CreateIndex(
				"IX_FlatRates_Id",
				"FlatRates",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_RentalLocations_Id",
				"RentalLocations",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_RentalProcesses_CustomerId",
				"RentalProcesses",
				"CustomerId");

			migrationBuilder.CreateIndex(
				"IX_RentalProcesses_EndLocationId",
				"RentalProcesses",
				"EndLocationId");

			migrationBuilder.CreateIndex(
				"IX_RentalProcesses_Id",
				"RentalProcesses",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_RentalProcesses_StartLocationId",
				"RentalProcesses",
				"StartLocationId");

			migrationBuilder.CreateIndex(
				"IX_RentalProcesses_VehicleId",
				"RentalProcesses",
				"VehicleId");

			migrationBuilder.CreateIndex(
				"IX_Reservations_CustomerId",
				"Reservations",
				"CustomerId");

			migrationBuilder.CreateIndex(
				"IX_Reservations_EndLocationId",
				"Reservations",
				"EndLocationId");

			migrationBuilder.CreateIndex(
				"IX_Reservations_Id",
				"Reservations",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_Reservations_PayedFlatRateId",
				"Reservations",
				"PayedFlatRateId");

			migrationBuilder.CreateIndex(
				"IX_Reservations_RequestedVehicletypeId",
				"Reservations",
				"RequestedVehicletypeId");

			migrationBuilder.CreateIndex(
				"IX_Reservations_StartLocationId",
				"Reservations",
				"StartLocationId");

			migrationBuilder.CreateIndex(
				"IX_Vehicles_CurrentRentalLocationId",
				"Vehicles",
				"CurrentRentalLocationId");

			migrationBuilder.CreateIndex(
				"IX_Vehicles_Id",
				"Vehicles",
				"Id");

			migrationBuilder.CreateIndex(
				"IX_Vehicles_OwnerRentalLocationId",
				"Vehicles",
				"OwnerRentalLocationId");

			migrationBuilder.CreateIndex(
				"IX_Vehicles_VehicletypeId",
				"Vehicles",
				"VehicletypeId");

			migrationBuilder.CreateIndex(
				"IX_Vehicletypes_Id",
				"Vehicletypes",
				"Id");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				"DriverLicenses");

			migrationBuilder.DropTable(
				"Reservations");

			migrationBuilder.DropTable(
				"RentalProcesses");

			migrationBuilder.DropTable(
				"FlatRates");

			migrationBuilder.DropTable(
				"Customers");

			migrationBuilder.DropTable(
				"Vehicles");

			migrationBuilder.DropTable(
				"RentalLocations");

			migrationBuilder.DropTable(
				"Vehicletypes");
		}
	}
}

#pragma warning restore CS1591 // Fehledes XML-Kommentar für öffentlich sichtbaren Typ oder Element