﻿using System.Linq;
using System.Threading.Tasks;
using ItsECars.Service.Entities.Base;

namespace ItsECars.Service.Repositories.Base
{
	/// <summary>
	/// Generic repository for the given <typeparamref name="TEntity"/>
	/// </summary>
	public interface IGenericRepository<TEntity> where TEntity : IEntity
	{
		/// <summary>
		/// Returns all available instances of the <typeparamref name="TEntity"/>>
		/// </summary>
		IQueryable<TEntity> GetAll();

		/// <summary>
		/// Returns the <typeparamref name="TEntity"/> which is identified by the given <paramref name="id"/>
		/// </summary>
		Task<TEntity> GetById(long id);

		/// <summary>
		/// Persists the given <paramref name="entity"/>
		/// </summary>
		Task Create(TEntity entity);

		/// <summary>
		/// Updates the fields of the given <paramref name="entity"/>
		/// </summary>
		Task Update(long id, TEntity entity);

		/// <summary>
		/// Deletes the <typeparamref name="TEntity"/>.
		/// </summary>
		Task Delete(long id);
	}
}
