﻿using System.Linq;
using System.Threading.Tasks;
using ItsECars.Service.Entities;
using ItsECars.Service.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace ItsECars.Service.Repositories.Base
{
	/// <summary>
	/// Base repository which offers basic CRUD functionality for the given <typeparamref name="TEntity"/>
	/// </summary>
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntity
	{
		/// <summary>
		/// Context which is used to access the database
		/// </summary>
		protected readonly ItsECarsDatabaseContext DatabaseContext;

		/// <summary>
		/// Creates a new instance of the <see cref="GenericRepository{TEntity}"/> class
		/// </summary>
		public GenericRepository(ItsECarsDatabaseContext databaseContext)
		{
			this.DatabaseContext = databaseContext;
		}

		/// <inheritdoc />
		public IQueryable<TEntity> GetAll()
		{
			return this.DatabaseContext.Set<TEntity>().AsNoTracking();
		}

		/// <inheritdoc />
		public async Task<TEntity> GetById(long id)
		{
			return await this.DatabaseContext.Set<TEntity>()
				.AsNoTracking()
				.FirstOrDefaultAsync(e => e.Id == id);
		}

		/// <inheritdoc />
		public async Task Create(TEntity entity)
		{
			await this.DatabaseContext.Set<TEntity>().AddAsync(entity);
			await this.DatabaseContext.SaveChangesAsync();
		}

		/// <inheritdoc />
		public async Task Update(long id, TEntity entity)
		{
			this.DatabaseContext.Set<TEntity>().Update(entity);
			await this.DatabaseContext.SaveChangesAsync();
		}

		/// <inheritdoc />
		public async Task Delete(long id)
		{
			var entity = await this.DatabaseContext.Set<TEntity>().FindAsync(id);
			this.DatabaseContext.Set<TEntity>().Remove(entity);
			await this.DatabaseContext.SaveChangesAsync();
		}
	}
}
