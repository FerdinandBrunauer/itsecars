﻿using System;
using System.IO;
using System.Reflection;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace ItsECars.Service
{
	/// <summary />
	public class Startup
	{
		/// <summary />
		public Startup(IConfiguration configuration)
		{
			this.Configuration = configuration;
		}

		/// <summary>
		/// Configuration of the application
		/// </summary>
		public IConfiguration Configuration { get; }

		/// <summary>
		/// This method gets called by the runtime. Use this method to add services to the container.
		/// </summary>
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			services.AddDbContext<ItsECarsDatabaseContext>(options => options.UseSqlite("Data Source=its_e_cars.db"));

			services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
			services.AddScoped(typeof(GenericRepository<>), typeof(GenericRepository<>));

			services.AddSwaggerGen(o =>
			{
				o.SwaggerDoc(
					"v1",
					new Info
					{
						Title = "ItsECars API",
						Description = "Sample API to try out some fancy things",
						Version = "v1",
						License = new License
						{
							Name = "MIT",
							Url = "https://opensource.org/licenses/MIT"
						},
						Contact = new Contact
						{
							Email = "ferdinand.brunauer@gmail.com",
							Name = "Ferdinand Brunauer"
						}
					}
				);

				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				o.IncludeXmlComments(xmlPath);
			});
		}

		/// <summary>
		/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		/// </summary>
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSwagger();
			app.UseSwaggerUI(o =>
			{
				o.SwaggerEndpoint("/swagger/v1/swagger.json", "ItsECars API");
				o.RoutePrefix = string.Empty;
			});

			app.UseCors("AllowAll");
			app.UseMvc();

			using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetRequiredService<ItsECarsDatabaseContext>();
				context.Database.Migrate();
			}
		}
	}
}
