﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// Reservation controller
	/// </summary>
	public class ReservationController : APagedCrudController<Reservation, GenericRepository<Reservation>>
	{
		/// <inheritdoc />
		public ReservationController(GenericRepository<Reservation> entityRepository) : base(entityRepository)
		{
		}
	}
}