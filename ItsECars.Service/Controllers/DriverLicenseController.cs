﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// DriverLicense controller
	/// </summary>
	public class DriverLicenseController : APagedCrudController<DriverLicense, GenericRepository<DriverLicense>>
	{
		/// <inheritdoc />
		public DriverLicenseController(GenericRepository<DriverLicense> entityRepository) : base(entityRepository)
		{
		}
	}
}