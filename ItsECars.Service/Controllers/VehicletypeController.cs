﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// Customer controller
	/// </summary>
	public class VehicletypeController : APagedCrudController<Vehicletype, GenericRepository<Vehicletype>>
	{
		/// <inheritdoc />
		public VehicletypeController(GenericRepository<Vehicletype> entityRepository) : base(entityRepository)
		{
		}
	}
}