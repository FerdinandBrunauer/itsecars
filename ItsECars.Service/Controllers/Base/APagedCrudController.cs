﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItsECars.Service.Entities.Base;
using ItsECars.Service.Repositories.Base;
using Microsoft.AspNetCore.Mvc;

namespace ItsECars.Service.Controllers.Base
{
	/// <summary>
	/// Basic controller which exposes all CRUD operations
	/// </summary>
	[Route("api/[controller]")]
	[Produces("application/json")]
	public abstract class APagedCrudController<TEntity, TRepository> : Controller
		where TEntity : class, IEntity
		where TRepository : IGenericRepository<TEntity>
	{
		/// <summary>
		/// Allows database access regarding the Entity
		/// </summary>
		protected readonly TRepository EntityRepository;

		/// <summary>
		/// Creates a new instance of the <see cref="APagedCrudController{TEntity,TRepository}"/> class
		/// </summary>
		protected APagedCrudController(TRepository entityRepository)
		{
			this.EntityRepository = entityRepository;
		}

		/// <summary>
		/// Creates the Entity
		/// </summary>
		[HttpPut]
		public async void CreateEntity([FromBody] TEntity entity)
		{
			await this.EntityRepository.Create(entity);
		}

		/// <summary>
		/// Returns all available objects of the Entities
		/// </summary>
		[HttpGet]
		public IEnumerable<TEntity> GetAllEntities([FromQuery] Pagination pagination)
		{
			return this.EntityRepository
				.GetAll()
				.OrderBy(e => e.Id)
				.Skip(pagination.Page * pagination.PerPage)
				.Take(pagination.PerPage);
		}

		/// <summary>
		/// Returns the Entity with the given <paramref name="id"/>
		/// </summary>
		[HttpGet("{id}")]
		public Task<TEntity> GetEntityById(long id)
		{
			return this.EntityRepository.GetById(id);
		}

		/// <summary>
		/// Updates the Entity
		/// </summary>
		[HttpPost("{id}"), HttpPut("{id}")]
		public async void UpdateEntity(long id, [FromBody] TEntity entity)
		{
			await this.EntityRepository.Update(id, entity);
		}

		/// <summary>
		/// Delete the given Entity from the database
		/// </summary>
		[HttpGet("delete/{id}")]
		public async void DeleteEntity(long id)
		{
			await this.EntityRepository.Delete(id);
		}
	}
}
