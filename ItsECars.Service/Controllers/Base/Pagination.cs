﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace ItsECars.Service.Controllers.Base
{
	/// <summary>
	/// Class which provides pagination information
	/// </summary>
	public class Pagination
	{
		private ushort perPage;

		/// <summary>
		/// Creates a new instance of the <see cref="Pagination"/> class
		/// </summary>
		public Pagination()
		{
			this.Page = 0;
			this.PerPage = 20;
		}

		/// <summary>
		/// Current page number
		/// </summary>
		[FromQuery(Name = "page")]
		[UsedImplicitly]
		public ushort Page { get; set; }

		/// <summary>
		/// Items per page
		/// </summary>
		[FromQuery(Name = "per_page")]
		[UsedImplicitly]
		public ushort PerPage
		{
			get => this.perPage;
			set
			{
				if (value > 100)
				{
					value = 100;
				}

				this.perPage = value;
			}
		}
	}
}
