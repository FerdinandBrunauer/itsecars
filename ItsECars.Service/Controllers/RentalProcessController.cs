﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// RentalProcess controller
	/// </summary>
	public class RentalProcessController : APagedCrudController<RentalProcess, GenericRepository<RentalProcess>>
	{
		/// <inheritdoc />
		public RentalProcessController(GenericRepository<RentalProcess> entityRepository) : base(entityRepository)
		{
		}
	}
}