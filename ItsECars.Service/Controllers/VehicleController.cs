﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// Vehicle controller
	/// </summary>
	public class VehicleController : APagedCrudController<Vehicle, GenericRepository<Vehicle>>
	{
		/// <inheritdoc />
		public VehicleController(GenericRepository<Vehicle> entityRepository) : base(entityRepository)
		{
		}
	}
}
