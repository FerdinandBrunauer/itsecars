﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// RentalLocation controller
	/// </summary>
	public class RentalLocationController : APagedCrudController<RentalLocation, GenericRepository<RentalLocation>>
	{
		/// <inheritdoc />
		public RentalLocationController(GenericRepository<RentalLocation> entityRepository) : base(entityRepository)
		{
		}
	}
}