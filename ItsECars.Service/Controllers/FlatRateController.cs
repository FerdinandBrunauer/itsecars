﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// FlatRate controller
	/// </summary>
	public class FlatRateController : APagedCrudController<FlatRate, GenericRepository<FlatRate>>
	{
		/// <inheritdoc />
		public FlatRateController(GenericRepository<FlatRate> entityRepository) : base(entityRepository)
		{
		}
	}
}