﻿using ItsECars.Service.Controllers.Base;
using ItsECars.Service.Entities;
using ItsECars.Service.Repositories.Base;

namespace ItsECars.Service.Controllers
{
	/// <summary>
	/// Customer controller
	/// </summary>
	public class CustomerController : APagedCrudController<Customer, GenericRepository<Customer>>
	{
		/// <inheritdoc />
		public CustomerController(GenericRepository<Customer> entityRepository) : base(entityRepository)
		{
		}
	}
}