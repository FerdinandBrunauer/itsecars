﻿using System;
using ItsECars.Service.Entities.Base;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents the interest of a <see cref="Customer"/> in a <see cref="Vehicletype"/>
	/// May result in a <see cref="RentalProcess"/>
	/// </summary>
	public class Reservation : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Requested start time
		/// </summary>
		public DateTime StartTime { get; set; }

		/// <summary>
		/// Requested end time
		/// </summary>
		public DateTime EndTime { get; set; }

		/// <summary>
		/// Start location of the <see cref="Reservation"/>
		/// </summary>
		public RentalLocation StartLocation { get; set; }

		/// <summary>
		/// End location of the <see cref="Reservation"/>
		/// </summary>
		public RentalLocation EndLocation { get; set; }

		/// <summary>
		/// Requested <see cref="Vehicletype"/>
		/// </summary>
		public Vehicletype RequestedVehicletype { get; set; }

		/// <summary>
		/// Already payed <see cref="FlatRate"/>
		/// </summary>
		public FlatRate PayedFlatRate { get; set; }

		/// <summary>
		/// Reservation done by <see cref="Customer"/>
		/// </summary>
		public Customer Customer { get; set; }

		/// <summary>
		/// Resulting <see cref="RentalProcess"/>
		/// </summary>
		public RentalProcess RentalProcess { get; set; }
	}
}