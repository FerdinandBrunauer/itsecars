﻿using System.Collections.Generic;
using ItsECars.Service.Entities.Base;
using Newtonsoft.Json;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a bookable <see cref="Vehicle"/>
	/// </summary>
	public class Vehicle : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Current mileage of the <see cref="Vehicle"/>
		/// </summary>
		public int CurrentMileage { get; set; }

		/// <summary>
		/// Fix costs per rent
		/// </summary>
		public float FixCostPerRental { get; set; }

		/// <summary>
		/// Cost per kilometer during a rent
		/// </summary>
		public float CostPerMileage { get; set; }

		/// <summary>
		/// Cost per day during a rent
		/// </summary>
		public float CostPerDay { get; set; }

		/// <summary>
		/// Number plate of the <see cref="Vehicle"/>
		/// </summary>
		public string NumberPlate { get; set; }

		/// <summary>
		/// Current location of the <see cref="Vehicle"/>
		/// May be null if it is currently not available
		/// </summary>
		public RentalLocation CurrentRentalLocation { get; set; }

		/// <summary>
		/// Origin location of the <see cref="Vehicle"/>
		/// <see cref="Vehicle"/> should always be there if not booked
		/// </summary>
		public RentalLocation OwnerRentalLocation { get; set; }

		/// <summary>
		/// Type of the <see cref="Vehicle"/>
		/// </summary>
		public Vehicletype Vehicletype { get; set; }

		/// <summary>
		/// Rentals of this <see cref="Vehicle"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<RentalProcess> RentalProcesses { get; set; }
	}
}
