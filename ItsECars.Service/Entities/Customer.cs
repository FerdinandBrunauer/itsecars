﻿using System;
using System.Collections.Generic;
using ItsECars.Service.Entities.Base;
using Newtonsoft.Json;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a <see cref="Customer"/> which can book <see cref="Vehicle"/>
	/// </summary>
	public class Customer : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Firstname of the <see cref="Customer"/>
		/// </summary>
		public string Firstname { get; set; }

		/// <summary>
		/// Lastname of the <see cref="Customer"/>
		/// </summary>
		public string Lastname { get; set; }

		/// <summary>
		/// Birthdate of the <see cref="Customer"/>
		/// </summary>
		public DateTime Birthdate { get; set; }

		/// <summary>
		/// Living country of the <see cref="Customer"/>
		/// </summary>
		public string Country { get; set; }

		/// <summary>
		/// Living street name of the <see cref="Customer"/>
		/// </summary>
		public string StreetName { get; set; }

		/// <summary>
		/// Living street number of the <see cref="Customer"/>
		/// </summary>
		public string StreetNumber { get; set; }

		/// <summary>
		/// Living postal code of the <see cref="Customer"/>
		/// </summary>
		public string PostalCode { get; set; }

		/// <summary>
		/// Living vilage name of the <see cref="Customer"/>
		/// </summary>
		public string City { get; set; }

		/// <summary>
		/// <see cref="DriverLicense"/> of the <see cref="Customer"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<DriverLicense> DriverLicenses { get; set; }

		/// <summary>
		/// Open/Booked <see cref="Reservation"/> of the <see cref="Customer"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<Reservation> Reservations { get; set; }

		/// <summary>
		/// Open/Done <see cref="RentalProcess"/> of the <see cref="Customer"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<RentalProcess> RentalProcesses { get; set; }
	}
}