﻿using System.Collections.Generic;
using ItsECars.Service.Entities.Base;
using Newtonsoft.Json;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a <see cref="RentalLocation"/> where <see cref="Vehicle"/> can be picked up or brought back
	/// </summary>
	public class RentalLocation : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Textual representation of the <see cref="RentalLocation"/>
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Living country of the <see cref="Customer"/>
		/// </summary>
		public string Country { get; set; }

		/// <summary>
		/// Living street name of the <see cref="Customer"/>
		/// </summary>
		public string StreetName { get; set; }

		/// <summary>
		/// Living street number of the <see cref="Customer"/>
		/// </summary>
		public string StreetNumber { get; set; }

		/// <summary>
		/// Living postal code of the <see cref="Customer"/>
		/// </summary>
		public string PostalCode { get; set; }

		/// <summary>
		/// Living vilage name of the <see cref="Customer"/>
		/// </summary>
		public string City { get; set; }

		/// <summary>
		/// <see cref="Vehicle"/> which are currently at this <see cref="RentalLocation"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<Vehicle> CurrentlyAvailableVehicles { get; set; }

		/// <summary>
		/// <see cref="Vehicle"/> which belongs to this <see cref="RentalLocation"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<Vehicle> OwnedVehicles { get; set; }

		/// <summary>
		/// <see cref="Reservation"/> which started here
		/// </summary>
		[JsonIgnore]
		public ICollection<Reservation> StartedReservations { get; set; }

		/// <summary>
		/// <see cref="Reservation"/> which ended here
		/// </summary>
		[JsonIgnore]
		public ICollection<Reservation> EndedReservations { get; set; }

		/// <summary>
		/// <see cref="RentalProcess"/> which started here
		/// </summary>
		[JsonIgnore]
		public ICollection<RentalProcess> StartedRentalProcesses { get; set; }

		/// <summary>
		/// <see cref="RentalProcess"/> which ended here
		/// </summary>
		[JsonIgnore]
		public ICollection<RentalProcess> EndedRentalProcesses { get; set; }
	}
}