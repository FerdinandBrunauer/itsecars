﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a session to the ItsECars database and allows access to all tables
	/// </summary>
	[UsedImplicitly]
	public class ItsECarsDatabaseContext : DbContext
	{
		/// <summary>
		/// Creates a new instance of the <see cref="ItsECarsDatabaseContext"/> class
		/// </summary>
		public ItsECarsDatabaseContext(DbContextOptions<ItsECarsDatabaseContext> options) : base(options)
		{
		}

		/// <summary>
		/// All available <see cref="Vehicle"/>
		/// </summary>
		public DbSet<Vehicle> Vehicles { get; set; }

		/// <summary>
		/// All available <see cref="Vehicletype"/>
		/// </summary>
		public DbSet<Vehicletype> Vehicletypes { get; set; }

		/// <summary>
		/// All available <see cref="Customer"/>
		/// </summary>
		public DbSet<Customer> Customers { get; set; }

		/// <summary>
		/// All registered <see cref="DriverLicense"/>
		/// </summary>
		public DbSet<DriverLicense> DriverLicenses { get; set; }

		/// <summary>
		/// All available <see cref="FlatRate"/>
		/// </summary>
		public DbSet<FlatRate> FlatRates { get; set; }

		/// <summary>
		/// All requested <see cref="Reservations"/>
		/// </summary>
		public DbSet<Reservation> Reservations { get; set; }

		/// <summary>
		/// All available <see cref="RentalLocation"/>
		/// </summary>
		public DbSet<RentalLocation> RentalLocations { get; set; }

		/// <summary>
		/// All open/done <see cref="RentalProcess"/>
		/// </summary>
		public DbSet<RentalProcess> RentalProcesses { get; set; }

		/// <inheritdoc />
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			// Customer
			modelBuilder.Entity<Customer>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasMany(e => e.DriverLicenses).WithOne(e => e.Customer);
				entity.HasMany(e => e.Reservations).WithOne(e => e.Customer);
				entity.HasMany(e => e.RentalProcesses).WithOne(e => e.Customer);
			});

			// DriverLicense
			modelBuilder.Entity<DriverLicense>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasOne(e => e.Vehicletype).WithMany(e => e.DriverLicenses);
				entity.HasOne(e => e.Customer).WithMany(e => e.DriverLicenses);
			});

			// FlatRate
			modelBuilder.Entity<FlatRate>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasMany(e => e.Reservations).WithOne(e => e.PayedFlatRate);
			});

			// RentalLocation
			modelBuilder.Entity<RentalLocation>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasMany(e => e.CurrentlyAvailableVehicles).WithOne(e => e.CurrentRentalLocation);
				entity.HasMany(e => e.OwnedVehicles).WithOne(e => e.OwnerRentalLocation);
				entity.HasMany(e => e.StartedReservations).WithOne(e => e.StartLocation);
				entity.HasMany(e => e.EndedReservations).WithOne(e => e.EndLocation);
				entity.HasMany(e => e.StartedRentalProcesses).WithOne(e => e.StartLocation);
				entity.HasMany(e => e.EndedRentalProcesses).WithOne(e => e.EndLocation);
			});

			// RentalProcess
			modelBuilder.Entity<RentalProcess>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasOne(e => e.StartLocation).WithMany(e => e.StartedRentalProcesses);
				entity.HasOne(e => e.EndLocation).WithMany(e => e.EndedRentalProcesses);
				entity.HasOne(e => e.Vehicle).WithMany(e => e.RentalProcesses);
				entity.HasOne(e => e.Customer).WithMany(e => e.RentalProcesses);
				entity.HasOne(e => e.Reservation).WithOne(e => e.RentalProcess).HasForeignKey<Reservation>();
			});

			// Reservation
			modelBuilder.Entity<Reservation>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasOne(e => e.StartLocation).WithMany(e => e.StartedReservations);
				entity.HasOne(e => e.EndLocation).WithMany(e => e.EndedReservations);
				entity.HasOne(e => e.RequestedVehicletype).WithMany(e => e.Reservations);
				entity.HasOne(e => e.PayedFlatRate).WithMany(e => e.Reservations);
				entity.HasOne(e => e.Customer).WithMany(e => e.Reservations);
				entity.HasOne(e => e.RentalProcess).WithOne(e => e.Reservation);
			});

			// Vehicle
			modelBuilder.Entity<Vehicle>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasOne(e => e.CurrentRentalLocation).WithMany(e => e.CurrentlyAvailableVehicles);
				entity.HasOne(e => e.OwnerRentalLocation).WithMany(e => e.OwnedVehicles);
				entity.HasOne(e => e.Vehicletype).WithMany(e => e.Vehicles);
				entity.HasMany(e => e.RentalProcesses).WithOne(e => e.Vehicle);
			});

			// Vehicletype
			modelBuilder.Entity<Vehicletype>(entity =>
			{
				entity.HasKey(e => e.Id);
				entity.HasIndex(e => e.Id);
				entity.HasMany(e => e.Vehicles).WithOne(e => e.Vehicletype);
				entity.HasMany(e => e.DriverLicenses).WithOne(e => e.Vehicletype);
				entity.HasMany(e => e.Reservations).WithOne(e => e.RequestedVehicletype);
			});
		}
	}
}
