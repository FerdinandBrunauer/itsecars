﻿using System;
using ItsECars.Service.Entities.Base;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents the rental of a <see cref="Vehicle"/>
	/// May be the result of a previous <see cref="Entities.Reservation"/>
	/// </summary>
	public class RentalProcess : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Start time of the <see cref="RentalProcess"/>
		/// </summary>
		public DateTime StartTime { get; set; }

		/// <summary>
		/// End time of the <see cref="RentalProcess"/>
		/// May be null if the <see cref="RentalProcess"/> is not finished yet
		/// </summary>
		public DateTime? EndTime { get; set; }

		/// <summary>
		/// Start mileage of the <see cref="Vehicle"/>
		/// </summary>
		public int StartMileage { get; set; }

		/// <summary>
		/// End mileage of the <see cref="Vehicle"/>
		/// </summary>
		public int? EndMileage { get; set; }

		/// <summary>
		/// Start location
		/// </summary>
		public RentalLocation StartLocation { get; set; }

		/// <summary>
		/// End location
		/// </summary>
		public RentalLocation EndLocation { get; set; }

		/// <summary>
		/// Rented <see cref="Vehicle"/>
		/// </summary>
		public Vehicle Vehicle { get; set; }

		/// <summary>
		/// <see cref="Customer"/> who initiated this <see cref="RentalProcess"/>
		/// </summary>
		public Customer Customer { get; set; }

		/// <summary>
		/// Optional previous <see cref="Entities.Reservation"/>
		/// <see cref="Entities.Reservation"/> may already include payed <see cref="FlatRate"/>
		/// </summary>
		public Reservation Reservation { get; set; }
	}
}