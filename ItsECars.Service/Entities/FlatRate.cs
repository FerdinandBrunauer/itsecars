﻿using System.Collections.Generic;
using ItsECars.Service.Entities.Base;
using Newtonsoft.Json;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Booked and payed flat rate of a <see cref="Reservation"/>
	/// </summary>
	public class FlatRate : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Name of the <see cref="FlatRate"/>
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Included mileage in the <see cref="FlatRate"/>
		/// </summary>
		public int IncludedMileage { get; set; }

		/// <summary>
		/// Included days in the <see cref="FlatRate"/>
		/// </summary>
		public int IncludedDays { get; set; }

		/// <summary>
		/// Booked with <see cref="Reservation"/>
		/// </summary>
		[JsonIgnore]
		public ICollection<Reservation> Reservations { get; set; }
	}
}