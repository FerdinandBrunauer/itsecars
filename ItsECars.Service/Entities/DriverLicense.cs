﻿using System;
using ItsECars.Service.Entities.Base;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a <see cref="DriverLicense"/> of a <see cref="Customer"/>
	/// </summary>
	public class DriverLicense : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Driver license checked by
		/// </summary>
		public string CheckedBy { get; set; }

		/// <summary>
		/// Driver license checked at
		/// </summary>
		public DateTime? CheckedOn { get; set; }

		/// <summary>
		/// Allowed <see cref="Vehicletype"/> with this <see cref="DriverLicense"/>
		/// </summary>
		public Vehicletype Vehicletype { get; set; }

		/// <summary>
		/// <see cref="Customer"/> of this <see cref="DriverLicense"/>
		/// </summary>
		public Customer Customer { get; set; }
	}
}