﻿using System.Collections.Generic;
using ItsECars.Service.Entities.Base;
using Newtonsoft.Json;

namespace ItsECars.Service.Entities
{
	/// <summary>
	/// Represents a type of a <see cref="Vehicle"/>
	/// </summary>
	public class Vehicletype : IEntity
	{
		/// <inheritdoc />
		public long Id { get; set; }

		/// <summary>
		/// Name of the type
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Minimum number of seat places in this type
		/// </summary>
		public int SeatPlaces { get; set; }

		/// <summary>
		/// <see cref="Vehicle"/> with this type
		/// </summary>
		[JsonIgnore]
		public ICollection<Vehicle> Vehicles { get; set; }

		/// <summary>
		/// <see cref="Customer"/> which are allowed to drive this type
		/// </summary>
		[JsonIgnore]
		public ICollection<DriverLicense> DriverLicenses { get; set; }

		/// <summary>
		/// Open/Booked <see cref="Reservation"/> from this type
		/// </summary>
		[JsonIgnore]
		public ICollection<Reservation> Reservations { get; set; }
	}
}