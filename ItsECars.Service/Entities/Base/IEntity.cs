﻿namespace ItsECars.Service.Entities.Base
{
	/// <summary>
	/// Base interface for all Entities with an Id
	/// </summary>
	public interface IEntity
	{
		/// <summary>
		///     Id of the entity
		/// </summary>
		long Id { get; }
	}
}